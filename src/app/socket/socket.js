
angular.module( 'newsFeed.socket', [
  'ui.router',
  'plusOne'
])

.factory("socket", [
    "socketFactory", "appConstants", "$rootScope", 
    function(socketFactory, appConstants, $rootScope) {

        var socket = {

            isConnected: false,

            connect: function connect(sessionId) {
                var self = this;
                var url = appConstants.socket.url;
                var prefix = appConstants.socket.prefix;

                console.log("Connecting socket to " + (url || window.location.host));

                var ioSocket = io.connect(url, {
                    forceNew: true,
                    query: "session_id=" + sessionId
                });

                ioSocket.on("connect", function(){
                    console.log("Socket connection is successful");
                    self.isConnected = true;
                });

                ioSocket.on("error", function(error) {
                    if (error.type == "UnauthorizedError" || error.code == "invalid_token") {
                        console.log("User session has expired");
                        // fire this event for our app to react
                        $rootScope.$broadcast("user:timeout");
                    }
                });

                var socketInstance = socketFactory({
                    ioSocket: ioSocket,
                    prefix: prefix
                });

                // mixin
                jsface.extend(this, socketInstance);
            }

        };

        return socket;
    }
]);