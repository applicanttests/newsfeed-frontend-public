angular.module( 'NewsFeed', [
  'templates-app',
  'templates-common',
  'newsFeed.home',
  'ui.router',
  'btford.socket-io',
  'newsFeed.socket'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/home' );
})

.run( function run () {
})

.constant('appConstants',{
  server: '<SERVERNAME>',
  socket: {
    url: "<SERVERNAME>",
    prefix: ""
  }
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | NewsFeed' ;
    }
  });
})

;

